---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "一个正经的的知识库"
  text: "powered by Vitepress"
  tagline: 常用导航 & 开发经验 & 项目文档 ...

  actions:
    - theme: brand
      text: 常用导航
      link: /doc/navigation.md
      
    - theme: alt
      text: API Examples
      link: /doc/api-examples

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature B
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

